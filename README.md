```bash
docker run \
	--rm \
	-u $(id -u) \
	-v "$(realpath .)":/home/gradle/project \
	-w /home/gradle/project \
	gradle:7.1.1 \
	gradle wrapper
```


https://docs.gradle.org/current/userguide/what_is_gradle.html

https://docs.gradle.org/current/userguide/publishing_maven.html#publishing_maven:install
