package fraccions

data class Fraccio(val numerador: Long, val denominador: Long) {

    operator fun compareTo(other: Fraccio): Int = (numerador * other.denominador).compareTo(other.numerador * denominador)
}
