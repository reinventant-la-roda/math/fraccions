package fraccions

import kotlin.test.Test
import kotlin.test.assertTrue

class FraccioTest {

    @Test
    fun compare() {
        assertTrue { Fraccio(2,4) < Fraccio(3, 5) }
    }
}
